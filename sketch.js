var r, g, b
var variax
var variay
var variaDiamet

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function setup(){
    createCanvas(windowWidth, windowHeight)
    //createCanvas(600, 600)
    r = random(255)
    g = random(255)
    b = random(255)
    variax = getRandomArbitrary(0.1, 0.6)
    variay = getRandomArbitrary(0.05, 0.15)
    variaDiamet = getRandomArbitrary(1, 2.5)
}

function windowResized() {
	resizeCanvas(windowWidth, windowHeight)
}

function draw(){
    background(0)
    stroke(r, g, b)
    noFill()
    drawCircle(windowWidth / 2, windowHeight / 2, windowWidth/variaDiamet)
    //drawCircle(300, 300, 300)
}

function drawCircle(x, y, diamet){
    ellipse(x, y, diamet)
    if(diamet > 2){
        drawCircle(x + diamet * variax, y, diamet * variax)
        drawCircle(x - diamet * variax, y, diamet * variax)
        
        drawCircle(x, y + diamet * variay, diamet * variay)
        drawCircle(x, y - diamet * variay, diamet * variay)
    }
}

function mousePressed() {
      r = random(255)
      g = random(255)
      b = random(255)
      variax = getRandomArbitrary(0.1, 0.6)
      variay = getRandomArbitrary(0.05, 0.4)
      variaDiamet = getRandomArbitrary(1.5, 2.5)
}